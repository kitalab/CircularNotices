<?php
/**
 * View/Elements/CircularNotices/view_select_sortのテスト
 *
 * @author Noriko Arai <arai@nii.ac.jp>
 * @author Masaki Goto <go8ogle@gmail.com>
 * @link http://www.netcommons.org NetCommons Project
 * @license http://www.netcommons.org/license.txt NetCommons License
 * @copyright Copyright 2014, NetCommons Project
 */

App::uses('NetCommonsControllerTestCase', 'NetCommons.TestSuite');

/**
 * View/Elements/CircularNotices/view_select_sortのテスト
 *
 * @author Masaki Goto <go8ogle@gmail.com>
 * @package NetCommons\CircularNotices\Test\Case\View\Elements\CircularNotices\ViewSelectSort
 */
class CircularNoticesViewElementsCircularNoticesViewSelectSortTest extends NetCommonsControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array();

/**
 * Plugin name
 *
 * @var string
 */
	public $plugin = 'circular_notices';

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		//テストプラグインのロード
		NetCommonsCakeTestCase::loadTestPlugin($this, 'CircularNotices', 'TestCircularNotices');
		//テストコントローラ生成
		$this->generateNc('TestCircularNotices.TestViewElementsCircularNoticesViewSelectSort');
	}

/**
 * View/Elements/CircularNotices/view_select_sortのテスト
 *
 * @return void
 */
	public function testViewSelectSort() {
		$this->controller->set('circularNoticeContent', array('id' => 1));

		//テスト実行
		$this->_testGetAction('/test_circular_notices/test_view_elements_circular_notices_view_select_sort/view_select_sort',
				array('method' => 'assertNotEmpty'), null, 'view');

		//チェック
		$pattern = '/' . preg_quote('View/Elements/CircularNotices/view_select_sort', '/') . '/';
		$this->assertRegExp($pattern, $this->view);
	}
}
